import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetComponent } from './src/base-widget/base-widget.component';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    
	BaseWidgetComponent
  ],
  exports: [
    BaseWidgetComponent
  ]
})
export class DetailHorizontalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DetailHorizontalModule,
      providers: []
    };
  }
}
