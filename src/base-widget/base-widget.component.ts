import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detailhorizontal',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
  public DetailComponent: Array<any> = [
    {
        "title" : "Ketua Umum PBNU KH Said Aqil Siroj",
        "media_displayName" : "detik.com",
        "timestamp" : "2 days ago",
        "image" : "http://usimages.detik.com/community/media/visual/2016/12/30/9c5aef34-8d5e-43e8-82a4-937bcd73b18e_43.jpg?w=780&q=90",
        "content" : " Conservative candidate Francois Fillon is holding on to the third place in opinion polls despite a scandal over payments of public funds to his family that has hurt his campaign. The 63-year-old former prime minister proposes a supply-side economic strategy with cuts in public spending, loosening restrictions on the length of the working week, and raising the retirement age. He is also a social conservative who wants to limit adoption rights of gay couples and has called for warmer ties with Russia. Here are Fillon's other key policy proposals: ECONOMY AND EMPLOYMENT - Ending the 35-hour week: Fillon proposes a return to a legal working week of 39 hours in the public and private sectors, up from the 35-hour week which since 2000 has obliged employers to pay higher rates or give time off for hours above the 35-hour mark. Fillon says the 39-hour week would apply straight away in the public sector and that negotiated deals in the private sector can allow people to work up to an EU ceiling of 48 hours. - Public sector jobs: He proposes cutting the headcount by 500,000 by not replacing all retiring civil servants and increasing the working week to 39 hours. Some five million - or one in five of the workforce - are employed in the civil service, local government and �"
    }    
];
  constructor() { }

  ngOnInit() {
  }

}
